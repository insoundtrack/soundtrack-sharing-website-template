<!DOCTYPE html>
<html>
<head>
  <script src="https://anitklib.ml/js/jquery2.2.0.js"></script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/wavesurfer.js/1.0.52/wavesurfer.min.js"></script>
  <script src="access/JavaScript/main.js"></script>
  <link rel="stylesheet" href="access/CSS/mainStyle.css" />
  <meta charset="utf-8">
  <title>SoundTrack</title>
</head>
<body>

</body>
</html>

<?php
header('Access-Control-Allow-Origin: *');
include_once("config/db.php");

$query = mysql_query("SELECT * FROM public");

while ($rows = mysql_fetch_array($query)) {
  $ID = $rows['id'];
  $song_title = $rows['song_name'];
  $description = $rows['description'];
  $creater_name = $rows['creater'];
  $artwork = $rows['artwork'];
  $creater_img = $rows['creater_imgsrc'];
  $music_src = $rows['music_src'];

  echo '<div class="showcase">';


  //Playing music using JavaScript


  echo '<script>';
  echo
  "
  function buildInPlayer(){
    $(document).ready(function() {


      var audio = WaveSurfer.create({
          container: '.Track$ID',
          waveColor: '#d7d7d7',
          progressColor: '#e79110',
      });

      audio.load('$music_src');

      $('.Track$ID.playPause').click(function(){
        audio.playPause();
      });

      $('.Track$ID.showmusic').click(function(){
        audio.play();
      });

      $('.Track$ID.volume').on('input',function(){


        var vol = '0.' + $(this).val();



        audio.setVolume(vol);

      });


      $('.Track$ID.mute').click(function(){
        audio.toggleMute();
      });





    });

  }//END of build in player

  buildInPlayer();

  ";
  echo '</script>';

  echo '<script class="showscript"></script>';


  // Show the user profile image
  echo '<div class="creater_img" style="background-image:url('."'".$creater_img."'".'); background-size: 100% 100%;">' . '</div>';

  //Show the creater name
  echo '<p class="creater_name">' .$creater_name. '</p>';

  //Show the artwork

  echo '<div class="artwork" style="background-image:url('."'".$artwork."'".'); background-size: 100% 100%;">' . '</div>';

  //Show the music name
  echo '<p class="song_title">' .$song_title. '</p>';

  //Show the music
  echo '<div class="showmusic Track'.$ID.'">' . '</div>';

  //Show some controls penal

  echo '<a class="playPause Track'.$ID.'">Play|Pause' . '</a>';  //Cretae a play pause button

  echo '<input type="range" class="volume Track'.$ID.'" min="0" max="9" value="9" step="1"/>'; //Create a volume bar

  echo '<a class="mute Track'.$ID.'">Muted' . '</a>';
  echo '<a class="download Track'.$ID.'" href="'.$music_src.'" download>Download' . '</a>';


  echo '</div>';
}
?>
