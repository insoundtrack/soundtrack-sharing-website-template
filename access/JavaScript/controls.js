function buildInPlayer(){
  $(document).ready(function() {


    var audio = WaveSurfer.create({
        container: '.Track$ID',
        waveColor: '#d7d7d7',
        progressColor: '#e79110',
    });

    audio.load('$music_src');

    $('.Track$ID.playPause').click(function(){
      audio.playPause();
    });

    $('.Track$ID.showmusic').click(function(){
      audio.play();
    });

    $('.Track$ID.volume').on('input',function(){


      var vol = '0.' + $(this).val();



      audio.setVolume(vol);

    });


    $('.Track$ID.mute').click(function(){
      audio.toggleMute();
    });





  });

}//END of build in player

buildInPlayer();
